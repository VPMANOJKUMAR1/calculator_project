# Simple Calculator Project
## Project Description
+ Calculator function like a normal calculator.
+ C means clear. When a user clicks it, it should clear everything and go back to the first state it was in when the page loaded.
+ Back arrow is like pressing backspace; it'll delete the last character typed.
+ when user it on '+','-','*','/' and '=' it should perform normal mathemetical operation.
## Technologies Used
+ Used the HTML, CSS and JavaScript to build the Project.
## How to Install and Run the Project
+ To run the project just need any internet server. I will provide the link of project you can run on any server like, chrome, firefox etc.
+ Used the node.js to run the code.
+ Used the code editor called  vs code to write the code.
+ [link of the project](https://cheerful-crumble-44a956.netlify.app).
## Credits
+ Followed the Frontend Masters , a course by Brain Holt to accomplish the Project.
+ [link to web page](https://frontendmasters.com/courses/web-development-v3/).
